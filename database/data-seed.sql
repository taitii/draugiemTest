INSERT INTO tests (id, name)
VALUES
	(1, 'Cars'),
	(2, 'Fishing'),
	(3, 'IT');

INSERT INTO questions (id, question, test_id)
VALUES (1, 'WHeres audi made?', 1);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('France',false,1),
	('Lithuania',false,1),
	('Germany',true,1);


INSERT INTO questions (id, question, test_id)
VALUES (2, 'Wheres WV is made?', 1);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Niggeria',false,2),
	('Poland',false,2),
	('USA',false,2),
	('Germany',true,2);

INSERT INTO questions (id, question, test_id)
VALUES (3, 'Which cars are made in germany (You can choose multiple answers)?', 1);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Bently',false,3),
	('Audi',true,3),
	('VW',true,3),
	('Ferrari',false,3),
	('FORD',false,3),
	('Porshce',true,3),
	('BMW',true,3);

INSERT INTO questions (id, question, test_id)
VALUES (4, 'Which cars are made in italy?', 1);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Dodge',false,4),
	('Fiat',true,4),
	('Ferrari',true,4),
	('Opel',false,4),
	('Pagani',true,4),
	('Alfa romeo',true,4),
	('Ford',false,4);

INSERT INTO questions (id, question, test_id)
VALUES (5, 'Which car manufacturer acquired Bentley in 1931?', 1);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Porshce',false,5),
	('Masserati',false,5),
	('ALfa romeo',false,5),
	('Rolls-Royce',true,5);

INSERT INTO questions (id, question, test_id)
VALUES (6, 'What fishes are the predatory ones (You can choose multiple answers)?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Perch',true,6),
	('Carp',false,6),
	('Pike',true,6),
	('Bream',false,6),
	('Sudak',true,6);

INSERT INTO questions (id, question, test_id)
VALUES (7, 'What fishes are the predatory ones (You can choose multiple answers)?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Perch',true,6),
	('Carp',false,6),
	('Pike',true,6),
	('Bream',false,6),
	('Sudak',true,6);

INSERT INTO questions (id, question, test_id)
VALUES (8, 'How many of these fishes are carp related (YOu can choose multiple answers)?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Crucian carp', true, 8),
	('Tench', true, 8),
	('Roach', true, 8),
	('Salamon', false, 8),
	('Cod', false, 8),
	('Catfish', false, 8),
	('Eelpout', false, 8);

INSERT INTO questions (id, question, test_id)
VALUES (9, 'Which company sells fishing equipement in Baltic states?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('ESMA', false, 9),
	('Kosadka', false, 9),
	('Copeslietas', false, 9),
	('SALMO', true, 9);

INSERT INTO questions (id, question, test_id)
VALUES (10, 'Which of these lanes are braided?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Fireline', true, 10),
	('Salmo mono', false, 10),
	('Fluorline', false, 10);

INSERT INTO questions (id, question, test_id)
VALUES (11, 'What equipment do you need to go fishing(YOu can choose multiple answers)?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('Rod', true, 11),
	('Reel', true, 11),
	('Beer', false, 11),
	('Gloves', false, 11),
	('Boots', true, 11),
	('Fishin line', true, 11),
	('Lures', true, 11),
	('Bait', true, 11);

INSERT INTO questions (id, question, test_id)
VALUES (12, 'Whats the record of Latvia for catching catfish?', 2);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('100+kg', true, 12),
	('80kg', false, 12),
	('93kg', false, 12),
	('200kg', false, 12),
	('56kg', false, 12);

INSERT INTO questions (id, question, test_id)
VALUES (13, 'Which of the  languages are functional(YOu can choose multiple answers)?', 3);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('C#', true, 13),
	('Agda', true,13),
	('Joy', true, 13),
	('Mercury', true, 13),
	('Cascading Style Sheets(CSS)', false, 13),
	('XML', false, 13),
	('Python', false, 13);

INSERT INTO questions (id, question, test_id)
VALUES (14, 'Which one of these are JS frameworks(YOu can choose multiple answers)?', 3);
INSERT INTO answers (answer, correct, question_id)
VALUES	
	('Backbone', true, 14),
	('react', true, 14),
	('SASS', false, 14),
	('Lavarel', false, 14),
	('Django', false, 14),
	('Vue', true, 14),
	('Angular', false, 14),
	('Rails', false, 14);

INSERT INTO questions (id, question, test_id)
VALUES (15, 'How many basic datatypes RUBY has?', 3);
INSERT INTO answers (answer, correct, question_id)
VALUES	
	('six', true, 15),
	('three', false, 15),
	('eight', false, 15),
	('seven', false, 15),
	('two', false, 15);

INSERT INTO questions (id, question, test_id)
VALUES (16, 'Which one are CSS frameworks(YOu can choose multiple answers)?', 3);
INSERT INTO answers (answer, correct, question_id)
VALUES
	('bulma', true, 16),
	('bootstrap', true, 16),
	('Ember', false, 16),
	('Django', false, 16),
	('Meteor', false, 16);