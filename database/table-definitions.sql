CREATE TABLE tests (
    id int unsigned PRIMARY KEY  AUTO_INCREMENT NOT NULL,
    name varchar(255)
);

CREATE TABLE questions (
    id int unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
    question varchar(255),
    test_id int unsigned NOT NULL
);

CREATE TABLE answers (
    id int unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
    answer varchar(255),
    correct boolean default(false) NOT NULL,
    question_id int unsigned NOT NULL
);

CREATE TABLE test_instance (
    id int unsigned PRIMARY KEY AUTO_INCREMENT NOT NULL,
    user_name varchar(255),
    test_id int unsigned NOT NULL
);




