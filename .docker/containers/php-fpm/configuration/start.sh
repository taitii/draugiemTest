
# wait for mount and run composer install and npm install
printf "Waiting for volume mount..."
until [ -d ".docker" ]
do
    printf "."
    sleep 1
done
echo "Volume mounted!"


### DO THINGS AFTER SERVER IS STARTED AND VOLUMES MOUNTED TO HOST....


# # Run composer install if vendor directory is missing
# if [ ! -d "vendor" ]; then
#     /bin/composer.phar install
# fi

# # Run npm install if node_modules directory is missing
# if [ ! -d "node_modules" ]; then
#     npm install
# fi

php-fpm